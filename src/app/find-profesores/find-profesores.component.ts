import { Component, OnInit } from '@angular/core';
import { FindProfesor } from './find-profesor';
import { FindProfesorService } from './find-profesor.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-find-profesores',
  templateUrl: './find-profesores.component.html'
})

export class FindProfesoresComponent implements OnInit {

  findProfesor: FindProfesor = new FindProfesor();

  constructor(private findProfesorService: FindProfesorService,
              private router: Router,
              private activatedRoute: ActivatedRoute
              ) { }

  ngOnInit(): void {
    this.cargarProfesor();
  }

  cargarProfesor(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if(id){
        this.findProfesorService.findProfesor(id).subscribe( (findProfesor) => this.findProfesor = findProfesor)
      }
    });
  }
}
