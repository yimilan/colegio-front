export class FindProfesor {    
    idProfesor: number;
    idCursos: number[] = [];
    idAsignaturas: number[] = [];
    idEstudiantes: number[][];
    
    nombreProfesor: string;
    nombreCursos: string[] = [];
    nombreAsignaturas: string[] = [];
    nombreEstudiantes: string[][];
}
