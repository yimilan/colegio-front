import { FindProfesor } from './find-profesor'

export const FIND_PROFESORES: FindProfesor = {
    idProfesor: 1,
    nombreProfesor: "Julian",
    idCursos: [1, 2, 3, 4, 5],
    nombreCursos: ["1", "2", "3", "4", "5"],
    idAsignaturas: [1, 2, 3, 4, 5],
    nombreAsignaturas: ["1", "2", "3", "4", "5"],
    idEstudiantes: [[1, 2, 3, 4, 5], [1, 2, 3],[2, 3, 4, 5],[1, 2, 3],[6,4]],
    nombreEstudiantes: [["1", "2", "3", "4", "5"],["1", "2", "3"],["2","3","4","5"],["1", "2", "3"],["6","4"]]
};