import { Injectable } from '@angular/core';
import { FindProfesor } from './find-profesor'
import { FIND_PROFESORES } from './find-profesor.json'
import { of, Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class FindProfesorService {
  private urlEndPoint:string = 'http://localhost:8080/api/find'
  constructor(private http: HttpClient) { }
    
  findProfesor(id):  Observable<FindProfesor>{
    //return of(PROFESORES);
    return this.http.get<FindProfesor>(`${this.urlEndPoint}/${id}`);
  }
}
