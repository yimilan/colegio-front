import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { ProfesoresComponent } from './profesores/profesores.component';
import { ProfesorService } from './profesores/profesor.service';
import { FindProfesoresComponent } from './find-profesores/find-profesores.component';
import { FindProfesorService } from './find-profesores/find-profesor.service';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: '/profesores', pathMatch: 'full'},
  {path: 'profesores', component: ProfesoresComponent},
  {path: 'profesores/:id', component: FindProfesoresComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ProfesoresComponent,
    FindProfesoresComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    ProfesorService,
    FindProfesorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
