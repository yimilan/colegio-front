import { Component, OnInit } from '@angular/core';
import { Profesor } from './profesor'
import { ProfesorService } from './profesor.service'

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html'
})
export class ProfesoresComponent implements OnInit {

  profesores: Profesor;
  
  constructor(private profesorService: ProfesorService) { }

  ngOnInit(): void {
    this.profesorService.getProfesores().subscribe(
      profesores => this.profesores = profesores
    );
  }

}
