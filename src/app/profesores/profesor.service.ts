import { Injectable } from '@angular/core';
import { Profesor } from './profesor'
import { PROFESORES } from './profesores.json'
import { of, Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  private urlEndPoint:string = 'http://localhost:8080/api/find'
  constructor(private http: HttpClient) { }

  getProfesores():  Observable<Profesor>{
    //return of(PROFESORES);
    return this.http.get<Profesor>(this.urlEndPoint)
  }
}
